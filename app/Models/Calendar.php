<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    use HasFactory;

    protected $appends = [
        'classNames'
    ];

    protected function getClassNamesAttribute()
    {
        return json_decode($this->classes);
    }
}
