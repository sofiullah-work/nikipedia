<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Media extends BaseMedia implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $table = 'media';

    protected $appends = [
        'file_url',
        'media_path'
    ];

    // public function registerMediaConversions(Media $media = null): void
    // {
    //     $this
    //         ->addMediaConversion('preview')
    //         ->fit(Fit::Contain, 300, 300)
    //         ->nonQueued();
    // }

    protected function fileUrl(): Attribute
    {
        return new Attribute(
            get: fn () => url('media/'.$this->id.'/'.$this->file_name)
        );
    }

    protected function mediaPath(): Attribute
    {
        return new Attribute(
            get: fn () => '/media/'.$this->id.'/'.$this->file_name
        );
    }
}
