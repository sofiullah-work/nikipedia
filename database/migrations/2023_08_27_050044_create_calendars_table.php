<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->id();
            $table->string('groupId')->unique()->nullable();
            $table->boolean('allDay')->default(false);
            $table->dateTime('start');
            $table->dateTime('end')->nullable();
            $table->string('title')->nullable();
            // $table->string('url_event')->nullable();
            $table->json('classNames')->nullable();
            $table->boolean('editable')->default(false);
            $table->boolean('overlap')->default(true);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('calendars');
    }
};
