<?php

namespace Module\Calendar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use App\Models\Calendar;

class CalendarController extends Controller
{

    public function index()
    {
        return Inertia::render('Backend/Calendar/Index');
    }

    public function data(Request $request)
    {
        $user = Auth::user();
        $start = Carbon::parse($request->start)->format('Y-m-d');
        $end = Carbon::parse($request->end)->format('Y-m-d');

        $data = Calendar::whereBetween('start', [$start, $end])->where('created_by', $user->id)->get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $eventDate = Carbon::parse($request->eventDate)->format('Y-m-d');
        $startTime = $request->eventTime[0]['hours'].':'.$request->eventTime[0]['minutes'].':00';
        $endTime = $request->eventTime[1]['hours'].':'.$request->eventTime[1]['minutes'].':00';
        $start = $eventDate.' '.$startTime;
        $end = $eventDate.' '.$endTime;

        // $classArr = explode(",", $request->classes);
        $classes = json_encode($request->classes);

        $event = new Calendar();

        $event->start = $start;
        $event->end = $end;
        $event->title = $request->title;
        $event->description = $request->description;
        $event->created_by = $user->id;
        $event->classes = $classes;
        $event->save();

        return back();
    }

    public function show(string $id)
    {
        //
    }

    public function edit(string $id)
    {
        $event = Calendar::find($id);

        $data = [
            'eventDate'=> Carbon::parse($event->start)->format('Y-m-d'),
            'eventTime'=> [Carbon::parse($event->start)->format('H:i'), Carbon::parse($event->end)->format('H:i')],
            'title'=> $event->title,
            'description'=> $event->description,
            'classes'=> $event->classNames,
        ];

        return response()->json($data);
    }

    public function update(Request $request, string $id)
    {

    }

    public function destroy(string $id)
    {
        $event = Calendar::find($id);
        $event->delete();

        return redirect()->route('dash.calendar.index');
    }

}
