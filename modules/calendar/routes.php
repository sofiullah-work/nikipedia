<?php

use Illuminate\Support\Facades\Route;
use Module\Calendar\CalendarController;

Route::middleware([
    'web',
    'auth:sanctum',
    config('jetstream.auth_session')
])
->prefix('dashboard/')
->as('dash.')
->group(function () {

    Route::post('calendar/data', [CalendarController::class, 'data'])->name('calendar.data');
    Route::resource('calendar', CalendarController::class);

});
