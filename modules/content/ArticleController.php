<?php

namespace Module\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Inertia\Inertia;
use Illuminate\Support\Str;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $articles = Article::orderBy('created_at', 'DESC');

        if ($request->has('search') && !empty($request->search)) {
            $word = $request->search;
            $articles = $articles->where(function($q) use($word) {
                $q->where('title', 'LIKE', '%'.$word.'%')
                    ->orWhere('content', 'LIKE', '%'.$word.'%');
            });
        }

        $articles = $articles->paginate(20);

        return Inertia::render('Backend/Article/Index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Backend/Article/Form');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $article = new Article();

        $slug = !empty($request->url_title) ? $request->url_title : null;
        $title = $request->title;
        if (empty($slug)) {
            $slug = Str::of($title)->slug('-');
        }
        $slugCount = Article::where('url_title', 'LIKE', $slug.'%')->count();
        if ($slugCount > 0) {
            $slug = $slug.'-'.$slugCount;
        }

        $article->title = $title;
        $article->url_title = $slug;
        $article->meta_keywords = $request->meta_keywords;
        $article->meta_description = $request->meta_description;
        $article->excerpt = $request->excerpt;
        $article->content = $request->content;
        $article->status = $request->status;
        $article->featured_img = $request->featured_img;
        $article->save();

        return redirect()->route('dash.content.article.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $article = Article::find($id);

        return Inertia::render('Backend/Article/Form', ['article' => $article]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $article = Article::find($id);

        $slug = !empty($request->url_title) ? $request->url_title : null;
        $title = $request->title;
        if (empty($slug)) {
            $slug = Str::of($title)->slug('-');
        }
        $slugCount = Article::where('url_title', 'LIKE', $slug.'%')->where('id', '!=', $id)->count();
        if ($slugCount > 0) {
            $slug = $slug.'-'.$slugCount;
        }

        $article->title = $title;
        $article->url_title = $slug;
        $article->meta_keywords = $request->meta_keywords;
        $article->meta_description = $request->meta_description;
        $article->excerpt = $request->excerpt;
        $article->content = $request->content;
        $article->status = $request->status;
        $article->featured_img = $request->featured_img;
        $article->save();

        return redirect()->route('dash.content.article.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $article = Article::find($id);
        $article->delete();

        return redirect()->route('dash.content.article.index');
    }

    public function multipleDestroy(Request $request)
    {
        $ids = $request->ids;
        foreach ($ids as $id) {
            $article = Article::find($id);
            $article->delete();
        }
        return redirect()->route('dash.content.article.index');
    }
}
