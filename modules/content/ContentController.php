<?php

namespace Module\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Inertia\Inertia;

use App\Models\Content;

class ContentController extends Controller
{
    public function home()
    {
        return Inertia::render('Backend/Content/Home');
    }

    public function about()
    {
        return Inertia::render('Backend/Content/About');
    }
}
