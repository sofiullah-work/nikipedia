<?php

use Illuminate\Support\Facades\Route;
use Module\Content\ArticleController;
use Module\Content\ContentController;

Route::middleware([
    'web',
    'auth:sanctum',
    config('jetstream.auth_session')
])
->prefix('dashboard/content')
->as('dash.content.')
->group(function () {

    Route::get('/', [ContentController::class, 'home'])->name('home');
    Route::get('/about', [ContentController::class, 'about'])->name('about');

    Route::post('/article/multiple',[ArticleController::class, 'multipleDestroy'])->name('article.multipleDelete');
    Route::resource('article', ArticleController::class);

});
