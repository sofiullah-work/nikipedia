<?php

namespace Module\Downloadable;

use Illuminate\Support\ServiceProvider;

class DownloadableProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
