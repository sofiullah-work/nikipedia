<?php

namespace Module\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Media;

class AjaxController extends Controller
{

    public function getMediaList(Request $request)
    {
        $medias = Media::orderBy('created_at', 'DESC');

        if ($request->has('search') && !empty($request->search)) {
            $keyword = $request->search;
            $medias = $medias->where(function($q) use($keyword) {
                $q->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('file_name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('collection_name', 'LIKE', '%'.$keyword.'%');
            });
        }

        $medias = $medias->paginate(20);

        return response()->json($medias);
    }

}
