<?php

namespace Module\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Media;
// use Spatie\MediaLibrary\MediaCollections\Models\Media;
use App\Models\FileManager;
use Illuminate\Support\Facades\Log;

class GeneralController extends Controller
{

    public function media(Request $request)
    {
        $medias = Media::orderBy('created_at', 'DESC');

        if ($request->has('search') && !empty($request->search)) {
            $keyword = $request->search;
            $medias = $medias->where(function($q) use($keyword) {
                $q->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('file_name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('collection_name', 'LIKE', '%'.$keyword.'%');
            });
        }

        $medias = $medias->paginate(20)->appends($request->except('paginate'));

        return Inertia::render('Backend/Media/Index', ['medias' => $medias]);
    }

    public function uploadMedia(Request $request)
    {
        ini_set('upload_max_filesize', 2048);
        ini_set('post_max_size', 2048);
        if ($request->hasFile('medias')) {
            foreach ($request->file('medias') as $item) {
                $file = $item;
                $media = new Media();
                $media = $media->addMedia($file)->usingFileName($file->getClientOriginalName())->toMediaCollection('filemanager');
                $media->model_type = get_class(new Media);
                $media->save();

                $file->storeAs($media->id, $media->file_name, 'media');
            }
        }

        return back();
    }

    public function showMedia($id) {
        $media = Media::find($id);
        return response()->json($media);
    }

    public function getMediaList($type, Request $request)
    {
        $medias = Media::where('mime_type', 'LIKE', $type.'/%')->orderBy('created_at', 'DESC');

        if ($request->has('search') && !empty($request->search)) {
            $keyword = $request->search;
            $medias = $medias->where(function($q) use($keyword) {
                $q->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('file_name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('collection_name', 'LIKE', '%'.$keyword.'%');
            });
        }

        $medias = $medias->paginate(20)->appends($request->except('paginate'));

        return response()->json($medias);
    }

    public function uploadSingleMedia(Request $request)
    {
        ini_set('upload_max_filesize', 2048);
        ini_set('post_max_size', 2048);

        // dd($request->all());
        if ($request->hasFile('media')) {
            $file = $request->file('media')[0];
            $media = new Media();
            $media = $media->addMedia($file)->usingFileName($file->getClientOriginalName())->toMediaCollection('filemanager');
            $media->model_type = get_class(new Media);
            $media->save();

            $file->storeAs($media->id, $media->file_name, 'media');

            // return back()->with('media', $media);
            return back()->with('posted', $media);
            // return response(['media'=>$media]);
        }

        return back();
    }

}
