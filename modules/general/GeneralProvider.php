<?php

namespace Module\General;

use Illuminate\Support\ServiceProvider;

class GeneralProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->make('Module\General\GeneralController');
        $this->app->make('Module\General\AjaxController');
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }
}
