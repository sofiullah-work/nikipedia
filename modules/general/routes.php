<?php

use Illuminate\Support\Facades\Route;
use Module\General\AjaxController;
use Module\General\GeneralController;

Route::middleware([
    'web',
    'auth:sanctum',
    config('jetstream.auth_session')
])
->prefix('dashboard/general')
->as('dash.general.')
->group(function () {

    Route::prefix('media')
    ->as('media.')
    ->group(function() {
        Route::get('/', [GeneralController::class, 'media'])->name('index');
        Route::get('/ajax/{type}', [GeneralController::class, 'getMediaList'])->name('ajax');
        Route::post('/upload', [GeneralController::class, 'uploadMedia'])->name('upload');
        Route::get('/show/{id}', [GeneralController::class, 'showMedia'])->name('show');
        Route::post('/single-upload', [GeneralController::class, 'uploadSingleMedia'])->name('uploadSingle');
    });

});
