<?php

use Illuminate\Support\Facades\Route;
use Module\Tools\ToolsController;

Route::middleware([
    'web',
    'auth:sanctum',
    config('jetstream.auth_session')
])
->prefix('dashboard/')
->as('dash.')
->group(function () {

    Route::resource('tool', ToolsController::class);

});
